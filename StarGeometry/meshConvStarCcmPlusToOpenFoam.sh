#!/bin/bash

#	usage:
#	
#	first parameter is the path to the case folder
#	second parameter is the .ccm mesh file, as created by starccm+

#	~/OpenFOAM/fyna-3.0.1/run/testing/turboFlow_FJA/
#	cycloneMesh.ccm

#"$@"
#echo "$1"
#echo "$2"

#	echo "Hello my friend!"

if [ [ -z "$1" ] || [ -z "$2"] ] 
then 
	echo "no path or mesh file given: first argument is the path to the openFoam case and the second argument is .ccm mesh file to be converted."
else
	ccm26ToFoam -case "$1" "$2" 
	if [ $? -eq 0 ] 
		then
			echo "conversion successful. checking mesh." 
			cd "$1"
#			checkMesh 

			if [ $? -eq 0 ] 
				then
					echo "first mesh check successful. creating patches." 
					# createPatch -overwrite

					if [ $? -eq 0 ] 
						then
							echo "creating of patches successful. checking mesh."
							checkMesh 

							if [[ $? -eq 0 ]]; 
								then
									echo "second mesh check successful."
								else
									echo "second mesh check failed. aborting."
							fi

						else
							echo "creating of patches not successful. aborting."
					fi

				else
					echo "first meshcheck failed. aborting."
			fi

		else
			echo "conversion failed. aborting."
	fi
fi
